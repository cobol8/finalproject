       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL_1.
       AUTHOR. ARTHORN.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader8.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader8.rpt"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD TRADER-REPORT-FILE.
       01  DATA-REPORT             PIC X(44).

       FD  TRADER-FILE.
       01  TRADERS.
           88 END-OF-TRADER-FILE   VALUE HIGH-VALUES.
           05 TRADER-ALL           PIC X(12).
           05 TRADERS-ID           PIC X(2).
           05 TRADER-ID-MEMBER     PIC X(4).
           05 TRADER-PRICE         PIC X(6).
       WORKING-STORAGE SECTION. 
       01  SUM-PRICE               PIC 9(9).
       01  MEX-PRICE               PIC 9(9).
       01  TRADER.
           05  TRADER-ID               PIC X(2).
           05  MEMBER-ID               PIC X(4).
           05  PRICE                   PIC 9(6).
       PROCEDURE DIVISION.
       PROCESS-TRADER-FILE.
           OPEN INPUT  TRADER-FILE
           OPEN OUTPUT TRADER-REPORT-FILE 
           PERFORM READ-TRADER-FILE 
           PERFORM PROCESS-DATA UNTIL END-OF-TRADER-FILE 
              
           CLOSE TRADER-REPORT-FILE 
           CLOSE TRADER-FILE 
           GOBACK
           .
       PROCESS-DATA.
           MOVE TRADERS-ID TO TRADER-ID  
           MOVE TRADER-ID-MEMBER TO MEMBER-ID
           MOVE TRADER-PRICE TO PRICE
           WRITE DATA-REPORT FROM TRADER 
           
           .
       READ-TRADER-FILE.
           READ TRADER-FILE 
           AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ 
           .
       
       
           






           